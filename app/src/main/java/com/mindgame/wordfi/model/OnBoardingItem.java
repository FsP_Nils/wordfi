package com.mindgame.wordfi.model;

/***
 * @author Rodrigue Ngalani Touko
 *
 *
 */
public class OnBoardingItem {

    private final String description;
    private final int screenImage;

    public OnBoardingItem(int screenImage, String description) {
        this.description = description;
        this.screenImage = screenImage;
    }

    public String getDescription() {
        return description;
    }

    public int getScreenImage() {
        return screenImage;
    }
}
