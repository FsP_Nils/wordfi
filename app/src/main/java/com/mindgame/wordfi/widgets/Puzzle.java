package com.mindgame.wordfi.widgets;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;

import com.mindgame.wordfi.R;
import com.mindgame.wordfi.resources.WordDirection;
import com.mindgame.wordfi.services.Screen;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class Puzzle {

    private final LinearLayout puzzleBoard;
    private final List<String> words;
    private final int length[];
    private final int boardLength;
    private final TextView[][] grid;
    private final Drawable[] cellDesign;  // 0 for empty, 1 for player, 2 for friend, 3 for background
    private final Activity activity;
    private final float screenWith;


    public Puzzle(Activity activity, LinearLayout puzzleBoard, List<String> words){
        this.puzzleBoard = puzzleBoard;
        this.words = words;
        this.activity = activity;
        this.length = new int[8];
        this.boardLength =  8;
        this.grid = new TextView[boardLength][boardLength];
        this.cellDesign = new Drawable[4];
        this.screenWith = new Screen(activity).getWith();
    }

    public TextView[][] getGrid() {
        return grid;
    }

    public int getBoardLength() {
        return boardLength;
    }

    private void initialize(){
        loadResources();
        int cellSize = Math.round(screenWith/boardLength);
        int letterSize = cellSize / 5;
        LinearLayout.LayoutParams layoutParamsRow = new LinearLayout.LayoutParams(cellSize * boardLength, cellSize);
        LinearLayout.LayoutParams layoutParamsCell = new LinearLayout.LayoutParams(cellSize,cellSize);



        for (int i = 0; i < boardLength; i++){
            LinearLayout linearRow = new LinearLayout(activity);
            // make a row
            for(int j = 0; j < boardLength; j++) {
                grid[i][j] = new TextView(activity);
                grid[i][j].setText(" ");
                grid[i][j].setBackground(cellDesign[0]);
                grid[i][j].setTextColor(activity.getResources().getColor(R.color.secondary_color));
                grid[i][j].setGravity(Gravity.CENTER);
                grid[i][j].setTextSize(letterSize);
                linearRow.addView(grid[i][j],layoutParamsCell);
            }
            puzzleBoard.addView(linearRow,layoutParamsRow);
        }
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    private void loadResources() {
        cellDesign[0] = activity.getResources().getDrawable(R.color.transparentColor);// emptyLetters
        cellDesign[1] = activity.getResources().getDrawable(R.drawable.puzzle_cell);// hiddenLetters
        cellDesign[2] = activity.getResources().getDrawable(R.drawable.puzzle_cell); // foundByPlayer
        cellDesign[3] = activity.getResources().getDrawable(R.drawable.puzzle_cell);// foundByFriend
    }


    // Placing the first Word in the middle of the Board
    private  void placeFirstWord(String word){
        int yPosition = (grid.length - 1) / 2;
        int xPosition = (grid.length - word.length()) / 2;

        for(int i = 0; i < word.length() ; i++){
            grid[yPosition][xPosition].setText(String.valueOf(word.charAt(i)));
            grid[yPosition][xPosition].setBackground(cellDesign[1]);
            xPosition = xPosition + 1;
        }
    }


    // check if two points can fit in the Grid
    private  Boolean isValid(int i, int j){
        if (i >= 0 && i < grid.length && j >= 0 && j < grid.length) {
            return true;
        }
        return false;
    }

    // check if the char of the next word can match with a char in the Grid
    private  Boolean match(String word){
        for (int k = 0; word.charAt(k) != '\b'; k++) {
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid.length; j++) {
                    if (word.charAt(k) == grid[i][j].getText().charAt(0) || word.length() != 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // check if the word should fit horizontally or vertically
    private Boolean isAdjacent(int i, int j, int before, int after, WordDirection wordDirection){
        // first checking horizontally
        if (wordDirection == WordDirection.Horizontal) {
            int start = j - before;
            int end = j + after;
            // checking the validity of the -row side
            if (isValid(i - 1, 0)) {
                for (int col = start; col < end + 1; col++) {
                    if(col == j){
                        continue;
                    }
                    if(grid[i - 1][col].getText() != " " && grid[i][col].getText() != " "){
                        return true;
                    }
                }
            }
            // checking the validity of the +row side
            if (isValid(i + 1, 0)) {
                for(int col = start; col < end + 1; col++){
                    if(col == j){
                        continue;
                    }
                    if(grid[i + 1][col].getText() != " " && grid[i][col].getText() != " "){
                        return true;
                    }
                }
            }
        }
        // now we  can check Vertically
        if (wordDirection  == WordDirection.Vertical) {
            int start = i - before;
            int end = i + after;
            // checking the validity of the -column side
            if(isValid(0, j - 1)){
                for (int row = start; row < end + 1; row++) {
                    if(row == i){
                        continue;
                    }
                    if (grid[row][j - 1].getText() != " " && grid[row][j].getText() != " ") {
                        return true;
                    }
                }
            }
            // checking the validity of the +column side
            if (isValid(0, j + 1)) {
                for (int row = start; row < end + 1; row++) {
                    if(row == i){
                        continue;
                    }
                    if (grid[row][j + 1].getText() != " " && grid[row][j].getText() != " ") {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // check if a positioning is acceptable or not
    private Boolean isIllegal(String word){
        // check
        for (int i = 0; i < word.length(); i++) {
            for (int row = 0; row < grid.length; row++) {
                for (int col = 0; col < grid.length; col++) {
                    if(word.charAt(i) == grid[row][col].getText().charAt(0)){
                        if(isValid(row, col - 1) && isValid(row, col +1) && grid[row][col - 1].getText() == " " && grid[row][col + 1].getText() == " " && grid[row + 1 ][col].getText() == " "){
                            int before = i;
                            int after = word.length() - (i + 1);
                            if(isValid(row,col - before) && isValid(row, col + after)){
                                if(!isAdjacent(row, col, before, after, WordDirection.Horizontal)){
                                    return false;
                                }
                            }
                        }
                        if (isValid(row - 1, col) && isValid(row + 1, col) && grid[row - 1][col].getText() == " " && grid[row + 1][col].getText() == " " ) {
                            int before = i;
                            int after = word.length() - (i + 1);
                            if (isValid(row - before, col) && isValid(row + after, col)) {
                                if(!isAdjacent(row, col, before, after, WordDirection.Vertical)){
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    // checking if the current word will not be out of the Grid after adding in Grid
    private  Boolean isOutOfGrid(String word){
        for(int i = 0; i < word.length(); i++){
            for (int row = 0; row < grid.length; row++) {
                for (int col = 0; col < grid.length; col++) {
                    if(word.charAt(i) == grid[row][col].getText().charAt(0)){
                        // checking horizontally possibility
                        if(isValid(row, col - 1) && isValid(row, col + 1)){
                            int before = i;
                            int after = word.length() - (i + 1);
                            if(isValid(row, col - before) && isValid(row, col + after)){
                                return false;
                            }
                        }
                        // checking vertically possibility
                        if (isValid(row - 1, col) && isValid(row + 1, col)) {
                            int before = i;
                            int after = word.length() - (i + 1);
                            if (isValid(row - 1, col) && isValid(row + 1, col)) {
                                if(isValid(row - before, col) && isValid(row + after, col)){
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        /// the Word is out of Grid
        return true;
    }

    // check if the word should be Skip(ignore)
    private  int isSkippable(String word){
        if(!match(word)){
            // meaning that
            return 1;
        }
        if(isOutOfGrid(word)){
            // meaning that
            return 2;
        }
        if(isIllegal(word)){
            // meaning that
            return 3;
        }
        return 4;
    }

    // place the Word on the Grid at selected index
    private  void nowPlaceWord(int fix, int start, int end, WordDirection type, String word){
        if(type == WordDirection.Horizontal){
            int index = 0;
            for(int col = start; col < end + 1; col ++){
                grid[fix][col].setText(String.valueOf(word.charAt(index)));
                grid[fix][col].setBackground(cellDesign[1]);
                index = index + 1;
            }
        }
        if(type == WordDirection.Vertical){
            int index = 0;
            for(int row = start; row < end + 1; row++){
                grid[row][fix].setText(String.valueOf(word.charAt(index)));
                grid[row][fix].setBackground(cellDesign[1]);
                index = index + 1;
            }
        }
    }

    // check which place will be perfect for the current Word
    private  void placeWord(String word) {
        // for all possibility
        for(int i = 0; i < word.length(); i++){
            for(int row = 0; row < grid.length; row++){
                for (int col = 0; col < grid.length; col++) {
                    if(word.charAt(i) == grid[row][col].getText().charAt(0)){

                        // try to insert horizontal
                        if(isValid(row, col - 1) && isValid(row, col + 1) && grid[row][col - 1].getText() == " " && grid[row][col + 1].getText() == " " && grid[row - 1][col + 1].getText() == " " && grid[row + 1][col + 1].getText() == " " &&  grid[row][word.length() + 1].getText() == " " ){
                            int before = i;
                            int after = word.length() - (i + 1);
                            if(isValid(row, col - before) && isValid(row, col + after)){
                                if(!isAdjacent(row, col, before, after, WordDirection.Horizontal)){
                                    nowPlaceWord(row, col - before, col + after, WordDirection.Horizontal, word);
                                    return;
                                }
                            }
                        }

                        // try to insert vertical
                        if(isValid(row - 1, col) && isValid(row + 1, col) && grid[row - 1][col].getText() == " " && grid[row + 1][col].getText() == " " && grid[row + 1][col - 1].getText() == " "  && grid[row + 1][col + 1].getText() == " " &&  grid[word.length() + 1][col].getText() == " " ){
                            int before = i;
                            int after = word.length() - (i + 1);
                            if(isValid(row - before, col) && isValid(row + after, col)){
                                if(!isAdjacent(row, col, before, after, WordDirection.Vertical)){
                                    nowPlaceWord(col, row - before, row + after, WordDirection.Vertical, word);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // returning the index of the Maximum length String
    private int getMaxIndex(){
        int max = -1;
        int index = 0;
        for (int i = 0; i < 3; i++) {
            if (length[i] > max) {
                max = length[i];
                index = i;
            }
        }
        length[index] = -1;
        return index;
    }

    private static List<String> sortWordsByLength(List<String> words) {
        List<String> sortedWords = new ArrayList<>();
        int lenght = words.size();

        for(int i = 0; i < lenght; i++) {
            String longestWord = "";
            for (String word : words) {
                if(word.length() > longestWord.length()) {
                    longestWord = word;
                }
            }
            sortedWords.add(longestWord);
            words.remove(longestWord);
        }
        return sortedWords;
    }

    // perform the building of the Puzzle
    public  void load(List<String> words){
        initialize();
        List<String> sortedWords = sortWordsByLength(words);

        boolean isFirstWord = true;
        for(int i = 0; i < 3; i++){
            int index = getMaxIndex();
            if(isFirstWord)
            {
                isFirstWord = false;
                placeFirstWord(sortedWords.get(index));
            }
            else {
                int type = isSkippable(sortedWords.get(index));
                if(type == 1){
                    System.err.println("No matching letter");
                }else if (type ==2) {
                    System.err.println("Reaches outside of Board");
                }else if (type == 3) {
                    System.err.println("Illegal adjacency");
                }else{
                    placeWord(sortedWords.get(index));
                }
            }

        }

    }


}

