package com.mindgame.wordfi.widgets;

import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.mindgame.wordfi.resources.Level;

public class LettersBoard {

    //Letters Board
    private final TextView firstLetter, secondLetter, thirdLetter, fourthLetter, fifthLetter, sixthLetter;
    private final ConstraintLayout.LayoutParams firstLetterLayoutParams, secondLetterLayoutParams, thirdLetterLayoutParams,
                                                fourthLetterLayoutParams, fifthLetterLayoutParams, sixthLetterLayoutParams;
    private final TextView lettersCombination;
    private  Level level;


    public LettersBoard(TextView firstLetter, TextView secondLetter, TextView thirdLetter, TextView fourthLetter, TextView fifthLetter, TextView sixthLetter, TextView lettersCombination) {
        this.firstLetter = firstLetter;
        this.secondLetter = secondLetter;
        this.thirdLetter = thirdLetter;
        this.fourthLetter = fourthLetter;
        this.fifthLetter = fifthLetter;
        this.sixthLetter = sixthLetter;

        this.firstLetterLayoutParams = (ConstraintLayout.LayoutParams) firstLetter.getLayoutParams();
        this.secondLetterLayoutParams = (ConstraintLayout.LayoutParams) secondLetter.getLayoutParams();
        this.thirdLetterLayoutParams = (ConstraintLayout.LayoutParams) thirdLetter.getLayoutParams();
        this.fourthLetterLayoutParams = (ConstraintLayout.LayoutParams) fourthLetter.getLayoutParams();
        this.fifthLetterLayoutParams = (ConstraintLayout.LayoutParams) fifthLetter.getLayoutParams();
        this.sixthLetterLayoutParams = (ConstraintLayout.LayoutParams) sixthLetter.getLayoutParams();


        this.lettersCombination = lettersCombination;

        firstLetter.setOnClickListener(view -> addLetter(firstLetter));

        secondLetter.setOnClickListener(view -> addLetter(secondLetter));

        thirdLetter.setOnClickListener(view -> addLetter(thirdLetter));

        fourthLetter.setOnClickListener(view -> addLetter(fourthLetter));

        fifthLetter.setOnClickListener(view -> addLetter(fifthLetter));

        sixthLetter.setOnClickListener(view -> addLetter(sixthLetter));
    }


    // change Position of Letters on the Board
     public void load(String[] letters){
         Level level = Level.EASY;

         switch(letters.length){
             case 4:
                level = Level.NORMAL;
                break;

            case 5:
                level = Level.MEDIUM;
                break;

            case 6:
                level = Level.HARD;
                break;
        }

        switch (level) {
            case EASY:
                ((ViewGroup) fourthLetter.getParent()).removeView(fourthLetter);
                ((ViewGroup) fifthLetter.getParent()).removeView(fifthLetter);
                ((ViewGroup) sixthLetter.getParent()).removeView(sixthLetter);


                firstLetterLayoutParams.circleAngle = 0;
                firstLetter.setLayoutParams(firstLetterLayoutParams);
                firstLetter.setText(letters[0]);


                secondLetterLayoutParams.circleAngle = (float) 100;
                secondLetter.setLayoutParams(secondLetterLayoutParams);
                secondLetter.setText(letters[1]);


                thirdLetterLayoutParams.circleAngle = (float) 260;
                thirdLetter.setLayoutParams(thirdLetterLayoutParams);
                thirdLetter.setText(letters[2]);
                break;


            case NORMAL:
                ((ViewGroup) fifthLetter.getParent()).removeView(fifthLetter);
                ((ViewGroup) sixthLetter.getParent()).removeView(sixthLetter);

                firstLetterLayoutParams.circleAngle = 0;
                firstLetter.setLayoutParams(firstLetterLayoutParams);
                firstLetter.setText(letters[0]);

                secondLetterLayoutParams.circleAngle = 90;
                secondLetter.setLayoutParams(secondLetterLayoutParams);
                secondLetter.setText(letters[1]);

                thirdLetterLayoutParams.circleAngle = 180;
                thirdLetter.setLayoutParams(thirdLetterLayoutParams);
                thirdLetter.setText(letters[2]);

                fourthLetterLayoutParams.circleAngle = 270;
                fourthLetter.setLayoutParams(fourthLetterLayoutParams);
                fourthLetter.setText(letters[3]);
                break;


            case MEDIUM:
                ((ViewGroup) sixthLetter.getParent()).removeView(sixthLetter);

                firstLetterLayoutParams.circleAngle = 0;
                firstLetter.setLayoutParams(firstLetterLayoutParams);
                firstLetter.setText(letters[0]);

                secondLetterLayoutParams.circleAngle = 72;
                secondLetter.setLayoutParams(secondLetterLayoutParams);
                secondLetter.setText(letters[1]);

                thirdLetterLayoutParams.circleAngle = 144;
                thirdLetter.setLayoutParams(thirdLetterLayoutParams);
                thirdLetter.setText(letters[2]);

                fourthLetterLayoutParams.circleAngle = 216;
                fourthLetter.setLayoutParams(fourthLetterLayoutParams);
                fourthLetter.setText(letters[3]);

                fifthLetterLayoutParams.circleAngle = 288;
                fifthLetter.setLayoutParams(fifthLetterLayoutParams);
                fifthLetter.setText(letters[4]);
                break;


            case HARD:
                firstLetterLayoutParams.circleAngle = 0;
                firstLetter.setLayoutParams(firstLetterLayoutParams);
                firstLetter.setText(letters[0]);

                secondLetterLayoutParams.circleAngle = 60;
                secondLetter.setLayoutParams(secondLetterLayoutParams);
                secondLetter.setText(letters[1]);

                thirdLetterLayoutParams.circleAngle = 120;
                thirdLetter.setLayoutParams(thirdLetterLayoutParams);
                thirdLetter.setText(letters[2]);

                fourthLetterLayoutParams.circleAngle = 180;
                fourthLetter.setLayoutParams(fourthLetterLayoutParams);
                fourthLetter.setText(letters[3]);

                fifthLetterLayoutParams.circleAngle = 240;
                fifthLetter.setLayoutParams(fifthLetterLayoutParams);
                fifthLetter.setText(letters[4]);

                sixthLetterLayoutParams.circleAngle = 300;
                sixthLetter.setLayoutParams(sixthLetterLayoutParams);
                sixthLetter.setText(letters[5]);
        }

    }


    private void addLetter(TextView textView) {
        lettersCombination.setText(String.format("%s%s", lettersCombination.getText().toString(), textView.getText().toString()));
        if(lettersCombination.getText().length() == 9){
            // check the Answer
        }
        textView.setClickable(false);
    }

    public void reset(){
        lettersCombination.setText("");
        firstLetter.setClickable(true);
        secondLetter.setClickable(true);
        thirdLetter.setClickable(true);
        fourthLetter.setClickable(true);
        fifthLetter.setClickable(true);
        sixthLetter.setClickable(true);
    }

}
