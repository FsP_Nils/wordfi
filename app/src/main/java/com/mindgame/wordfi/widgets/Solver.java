package com.mindgame.wordfi.widgets;


import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.widget.TextView;

import com.mindgame.wordfi.R;
import com.mindgame.wordfi.controllers.activities.PlayActivity;

/***
 * @author Rodrigue Ngalani Touko
 *
 *
 */
public class Solver {

    private final int puzzleLength;
    private final TextView[][] grid;
    private final Activity activity;

    public Solver(Activity activity, Puzzle puzzle) {
        this.activity = activity;
        this.puzzleLength = puzzle.getBoardLength();
        this.grid = puzzle.getGrid();
    }

    // For searching left to right and top to bottom
     int[] x = { 0,1 };
     int[] y = { 1,0 };


     boolean isInGrid(TextView[][] grid, int row, int col, String word)
    {
        TextView[][] foundWord = new TextView[puzzleLength][puzzleLength];

        if (grid[row][col].getText().charAt(0) != word.charAt(0)) {
            return false;
        }
        else if(grid[row][col].getText().charAt(0) == word.charAt(0))
        {
            foundWord[row][col] = grid[row][col];

            for (int direction = 0; direction < 2; direction++) {

                int length;
                int rowDirection = row + x[direction];
                int columnDirection = col + y[direction];


                for (length = 1; length < word.length(); length++) {

                    if (rowDirection >= this.puzzleLength || rowDirection < 0 || columnDirection >= this.puzzleLength || columnDirection < 0)
                        break;

                    if (grid[rowDirection][columnDirection].getText().charAt(0) != word.charAt(length))
                        break;

                    foundWord[rowDirection][columnDirection] = grid[rowDirection][columnDirection];

                    // Moving in particular direction
                    rowDirection += x[direction];
                    columnDirection += y[direction];

                }


                /// TODO: Marquer a chaque fois le nombre de mot trouver pour savoir quand on est a la fin et ajuster le score
                // check if the word is Vertical in Grid
                if (length == word.length() && word.length() >= 3 && grid[row - 1][col].getText().charAt(0) == ' '  && grid[rowDirection][columnDirection].getText().charAt(0) == ' ' ) {
                    for (int i = 0; i < foundWord.length; i++) {
                        for (int j = 0; j < foundWord.length; j++) {
                            if( foundWord[i][j] != null  ) {
                                grid[i][j].setTextColor(activity.getColor(R.color.error_color));
                            }
                        }
                    }
                    return true;
                    // check if the word is Vertical in Grid
                }else if(length == word.length() && word.length() >= 3 && grid[row][col - 1].getText().charAt(0) == ' ' && grid[rowDirection][columnDirection - 1].getText().charAt(0) != ' ' && grid[rowDirection][columnDirection].getText().charAt(0) == ' '){
                    for (int i = 0; i < foundWord.length; i++) {
                        for (int j = 0; j < foundWord.length; j++) {
                            if( foundWord[i][j] != null) {
                                grid[i][j].setTextColor(activity.getColor(R.color.error_color));
                            }
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }




     public void check(String word)
    {
        if(!word.isEmpty()){
            for (int row = 0; row < puzzleLength; row++) {
                for (int col = 0; col < puzzleLength; col++) {
                    if (isInGrid(grid, row, col, word)) {
                        //TODO: Perform a sound effet
                        //TODO: increase the Score
                    }
                }
            }
        }

    }

    private void ScoreManager(int value){
    }

    public void help(){
         boolean helped = false;
         ColorStateList filledCaseColor = activity.getColorStateList(R.color.error_color);
         for (int i = 0; i < grid.length ; i++){
             for(int j = 0; j< grid.length; j++){
                 if(grid[i][j].getText() != " " && grid[i][j].getTextColors() != filledCaseColor && !helped){
                     grid[i][j].setTextColor(activity.getColor(R.color.error_color));
                     helped = true;
                 }
             }
         }
    }

}
