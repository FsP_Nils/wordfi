package com.mindgame.wordfi.resources;

public enum Level {
    /**
     * @author Nils..
     *
     * WordFi Available Level
     * [Easy] with 3 Characters Word,
     * [Normal] with 4 Characters Word,
     * [Medium] with 5 Characters Word,
     * [Hard] with 6 Characters Word,
     */
    EASY,
    NORMAL,
    MEDIUM,
    HARD;

    public static Level fromString(String value) {
        switch(value) {
            case "NORMAL":
                return NORMAL;
            case "MEDIUM":
                return MEDIUM;
            case "HARD":
                return HARD;
            default:
                return EASY;
        }
    }
}
