package com.mindgame.wordfi.resources;

/***
 * @author Nils
 */
public enum RoundMode {
    Local,
    Online;
}
