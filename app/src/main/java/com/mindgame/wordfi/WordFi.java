package com.mindgame.wordfi;

import android.app.Application;

import com.mindgame.wordfi.services.Settings;

import com.mindgame.wordfi.services.CoreData;
import com.parse.Parse;

/***
 * @author Rodrigue Ngalani Touko
 */
public class WordFi extends Application {

    private CoreData coreData;

    public void onCreate() {
        coreData = new CoreData(this);
        loadSetting();
        super.onCreate();
        initServer();
    }


    void initServer(){
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(getString(R.string.app_id))
                .clientKey(getString(R.string.client_key))
                .server(getString(R.string.server_url))
                .build()
        );
        //// Check Server Connection
        // ParseInstallation.getCurrentInstallation().saveInBackground();

    }

    /**
     * Load the Theme of the last Session
     */
    private void loadSetting() {
        Settings.setThemeTo(coreData.getTheme());
    }


}