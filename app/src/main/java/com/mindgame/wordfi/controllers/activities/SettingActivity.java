package com.mindgame.wordfi.controllers.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.mindgame.wordfi.R;
import com.mindgame.wordfi.services.Settings;
import com.mindgame.wordfi.services.CoreData;
import com.mindgame.wordfi.services.Screen;
import com.mindgame.wordfi.services.SoundManager;

/***
 * @author Nils
 */
public class SettingActivity extends AppCompatActivity   {

    private CoreData coreData;
    private TextView themeState, languageState, levelState, soundState, volumeState;
    private SwitchCompat soundSwitch;
    private String volumeValuePercentage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        //Enable Full Screen
        new Screen(this).fullMode();
        setContentView(R.layout.activity_setting);
        Settings.setLanguageTo(this,new CoreData(this).getLanguage());

        FrameLayout helpLinkLayout = findViewById(R.id.help_link);
        FrameLayout aboutUsLinkLayout = findViewById(R.id.about_us_link);
        MaterialButton backButton = findViewById(R.id.back_button);
        themeState = findViewById(R.id.theme_state);
        languageState = findViewById(R.id.language_state);
        levelState = findViewById(R.id.level_state);
        soundState = findViewById(R.id.sound_state);
        volumeState = findViewById(R.id.volume_state);

        Spinner themeSpinner = findViewById(R.id.theme_spinner);
        Spinner languageSpinner = findViewById(R.id.language_spinner);
        Spinner levelSpinner = findViewById(R.id.level_selector);
        SeekBar volumeSeekBar = findViewById(R.id.volume_manager);
        soundSwitch = findViewById(R.id.sound_switch);

        coreData = new CoreData(this);
        volumeValuePercentage =  coreData.getVolume() + "%";
        if(coreData.getSoundState()){
            soundState.setText(getText(R.string.sound_on));
        }else {
            soundState.setText(getText(R.string.sound_off));
        }


        themeSpinner.setSelection(coreData.getTheme());
        languageSpinner.setSelection(coreData.getLanguage());
        levelSpinner.setSelection(coreData.getLevel());
        soundSwitch.setChecked(coreData.getSoundState());
        volumeSeekBar.setProgress(coreData.getVolume());
        volumeState.setText(volumeValuePercentage);

        themeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                themeState.setText(parent.getItemAtPosition(position).toString());
                coreData.saveTheme(position);
                Settings.setThemeTo(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                languageState.setText(parent.getItemAtPosition(position).toString());
                int oldPosition = coreData.getLanguage();
                refreshLanguage(oldPosition,position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        levelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                levelState.setText(parent.getItemAtPosition(position).toString());
                coreData.saveLevel(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        soundSwitch.setOnClickListener(v -> {
            if (soundSwitch.isChecked() ){
                SoundManager.stopBackgroundMusic();
                SoundManager.startBackgroundMusic();
                coreData.saveSoundState(true);
                soundState.setText(getText(R.string.sound_on));
            }else {
                SoundManager.stopBackgroundMusic();
                coreData.saveSoundState(false);
                soundState.setText(getText(R.string.sound_off));
            }
        });

        volumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!coreData.getSoundState()){
                    coreData.saveSoundState(true);
                    soundState.setText(getText(R.string.sound_on));
                    soundSwitch.setChecked(coreData.getSoundState());
                }
                volumeValuePercentage = progress + "%";
                volumeState.setText(volumeValuePercentage);
                SoundManager.setVolumeTo(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                coreData.saveVolume(seekBar.getProgress());
            }

        });

        backButton.setOnClickListener(v -> {
            Intent intent=new Intent(this, DashboardActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });

        helpLinkLayout.setOnClickListener(v -> {
            Intent intent=new Intent(this, HelpActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });

        aboutUsLinkLayout.setOnClickListener(v -> {
            Intent intent=new Intent(this, AboutUsActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });
    }


    private void refreshLanguage(int currentPosition, int newPosition){
        if(currentPosition != newPosition){
            coreData.saveLanguage(newPosition);
            Settings.setLanguageTo(SettingActivity.this,newPosition);
            Intent intent=new Intent(this, SettingActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        }

    }


}