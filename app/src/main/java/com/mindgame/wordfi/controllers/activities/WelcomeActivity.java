package com.mindgame.wordfi.controllers.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

import com.google.android.material.button.MaterialButton;
import com.mindgame.wordfi.R;
import com.mindgame.wordfi.services.Settings;
import com.mindgame.wordfi.services.CoreData;
import com.mindgame.wordfi.services.Screen;

public class WelcomeActivity extends AppCompatActivity {

    public static MediaPlayer mediaPlayer;
    public static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Enable Full Screen
        new Screen(this).fullMode();
        setContentView(R.layout.activity_welcome);
        Settings.setLanguageTo(this,new CoreData(this).getLanguage());

        CoreData coreData = new CoreData(this);
        float volume = coreData.getVolume() / 100f;

        mediaPlayer = MediaPlayer.create(this, R.raw.ukulele);
        mediaPlayer.setLooping(true);
        if(coreData.getSoundState()){
            mediaPlayer.setVolume(volume,volume);
        }else {
            mediaPlayer.setVolume(0f,0f);
        }

        context = this;

        MaterialButton getStartedActionButton = (MaterialButton) findViewById(R.id.get_started_button);
        getStartedActionButton.setOnClickListener(view -> navigateToActivity());


    }

    private void navigateToActivity() {
      Intent intent =  new Intent(getApplicationContext(), DashboardActivity.class);
      startActivity(intent);
      mediaPlayer.start();
      finish();
      overridePendingTransition(0, 0);
    }

}