package com.mindgame.wordfi.controllers.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.mindgame.wordfi.R;
import com.mindgame.wordfi.services.Settings;
import com.mindgame.wordfi.widgets.LettersBoard;
import com.mindgame.wordfi.services.Screen;
import com.mindgame.wordfi.widgets.Puzzle;
import com.mindgame.wordfi.widgets.Solver;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

/***
 * @author Rodrigue Ngalani Touko
 *
 *
 */
public class PlayActivity extends AppCompatActivity {


    //Board Puzzle
    private LinearLayout puzzleBoard;
    private Puzzle puzzle;
    private Solver solver;
    private LettersBoard board;
    private MaterialButton leaveButton, helpButton;
    private ImageButton answerChecker;
    private TextView timeView, scoreView, selectedText;
    private TextView firstLetter, secondLetter, thirdLetter, fourthLetter, fifthLetter, sixthLetter, answerCheckButton;
    private Timer timer;
    private TimerTask timerTask;
    private Double time = 0.0;
    private int helpCounter = 0;

    private final String[] letters = {"T","E","A","M"};
    private final List<String> words = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Enable Full Screen
        new Screen(this).fullMode();
        setContentView(R.layout.activity_play);


        puzzleBoard = findViewById(R.id.puzzle_grid_board);
        answerChecker = findViewById(R.id.answer_checker);
        leaveButton = findViewById(R.id.leave_button);
        helpButton = findViewById(R.id.help_button);
        answerCheckButton = findViewById(R.id.check_answer_button);

        timeView = findViewById(R.id.timer_state);
        scoreView = findViewById(R.id.score_text_view);

        selectedText = findViewById(R.id.selected_letters);

        firstLetter = findViewById(R.id.letter_item_1);
        secondLetter = findViewById(R.id.letter_item_2);
        thirdLetter = findViewById(R.id.letter_item_3);
        fourthLetter = findViewById(R.id.letter_item_4);
        fifthLetter = findViewById(R.id.letter_item_5);
        sixthLetter = findViewById(R.id.letter_item_6);

        words.add("TEAM");
        words.add("TEA");
        words.add("EAT");
        words.add("MEAT");

        helpCounter = 0;
        timer = new Timer();
        puzzle = new Puzzle(this,puzzleBoard,words);
        puzzle.load(words);


        solver = new Solver(this,puzzle);

        board = new LettersBoard(firstLetter, secondLetter, thirdLetter, fourthLetter,fifthLetter,sixthLetter, selectedText );
        board.load(letters);

        leaveButton.setOnClickListener(v -> {
            Intent intent=new Intent(this, DashboardActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });

        answerCheckButton.setOnClickListener(view -> {
            solver.check(selectedText.getText().toString());
            board.reset();
        });

        helpButton.setOnClickListener(v -> puzzleHelp());

        startTimer();


    }

    /**
     *
     */
    private void startTimer()
    {
        timerTask = new TimerTask()
        {
            @Override
            public void run()
            {
                runOnUiThread(() -> {
                    time++;
                    timeView.setText(getTimerText());
                });
            }

        };
        timer.scheduleAtFixedRate(timerTask, 0 ,1000);
    }

    /**
     *
     * @return
     */
    private @NotNull String getTimerText()
    {
        int rounded = (int) Math.round(time);

        int seconds = ((rounded % 86400) % 3600) % 60;
        int minutes = ((rounded % 86400) % 3600) / 60;
        int hours = ((rounded % 86400) / 3600);

        return formatTime(seconds, minutes, hours);
    }

    /**
     *
     * @param seconds
     * @param minutes
     * @param hours
     * @return a
     */
    private @NotNull String formatTime(int seconds, int minutes, int hours)
    {
        final String format = String.format("%s : %s : %s",
                String.format("%02d", hours),
                String.format("%02d", minutes),
                String.format("%02d", seconds));
        return format;
    }

    /**
     *
     */
    private void puzzleHelp(){
        if (helpCounter < helpLength()){
            solver.help();
            helpCounter++;
            if(helpCounter == helpLength()){
                ((ViewGroup) helpButton.getParent()).removeView(helpButton);
            }
        }
    }

    /**
     *
     * @return number of possible help
     */
    private int helpLength(){
        String levelMode = Settings.getLevel(this);
        String[] levels = this.getResources().getStringArray(R.array.level);
        if(levelMode.equals(levels[1])){
            return 2;
        }else if(levelMode.equals(levels[2])){
            return 3;
        }else if(levelMode.equals(levels[3])){
            return 4;
        }else{
            return 1;
        }
    }


    
}