package com.mindgame.wordfi.controllers.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.material.button.MaterialButton;
import com.mindgame.wordfi.R;
import com.mindgame.wordfi.adapter.OnBoardingViewPagerAdapter;
import com.mindgame.wordfi.model.OnBoardingItem;
import com.mindgame.wordfi.services.Settings;
import com.mindgame.wordfi.services.CoreData;
import com.mindgame.wordfi.services.Screen;

import java.util.ArrayList;
import java.util.List;

/***
 * @author Rodrigue Ngalani Touko
 *
 *
 */
public class OnBoardingActivity extends AppCompatActivity {
    private OnBoardingViewPagerAdapter onBoardingViewPagerAdapter;
    private LinearLayout onBoardingIndicatorLayout;
    private MaterialButton onBoardingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Enable Full Screen
        new Screen(this).fullMode();
        setContentView(R.layout.activity_onboarding);

         Settings.setLanguageTo(this,new CoreData(this).getLanguage());


        onBoardingIndicatorLayout = findViewById(R.id.indicator_layout);
        setUpOnBoardingItems();

        ViewPager2 onBoardingViewPager = findViewById(R.id.view_pager2);
        onBoardingActionButton = findViewById(R.id.next_button);

        onBoardingViewPager.setAdapter(onBoardingViewPagerAdapter);

        setupOnBoardingIndicator();
        setCurrentViewIndicator(0);

        onBoardingViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setCurrentViewIndicator(position);
            }
        });

        onBoardingActionButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(onBoardingViewPager.getCurrentItem() + 1 < onBoardingViewPagerAdapter.getItemCount()){
                    onBoardingViewPager.setCurrentItem(onBoardingViewPager.getCurrentItem() + 1);
                } else {
                    startActivity(new Intent(getApplicationContext(), WelcomeActivity.class));
                    finish();
                    overridePendingTransition(0, 0);
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // change the isFirstStart State
    }

    private void setUpOnBoardingItems(){
        List<OnBoardingItem> onBoardingItems = new ArrayList<>();
        onBoardingItems.add(new OnBoardingItem(R.drawable.first_on_boarding_background,getString(R.string.first_onBoarding_message)));
        onBoardingItems.add(new OnBoardingItem(R.drawable.second_on_boarding_background,getString(R.string.second_onBoarding_message)));
        onBoardingItems.add(new OnBoardingItem(R.drawable.third_on_boardind_background,getString(R.string.third_onBoarding_message)));


        onBoardingViewPagerAdapter = new OnBoardingViewPagerAdapter(onBoardingItems);
    }

    private void setupOnBoardingIndicator(){
        ImageView[] indicators = new ImageView[onBoardingViewPagerAdapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );

        layoutParams.setMargins(8,0,8,0);

        for(int i = 0; i < indicators.length; i++){
            indicators[i] = new ImageView(getApplicationContext());
            indicators[i].setImageDrawable(ContextCompat.getDrawable(
                    getApplicationContext(),
                    R.drawable.shape_inactive_onboarding_indicator
            ));
            indicators[i].setLayoutParams(layoutParams);
            onBoardingIndicatorLayout.addView(indicators[i]);
        }
    }

    private void setCurrentViewIndicator(int index){
        int childCount = onBoardingIndicatorLayout.getChildCount();
        for(int i= 0; i< childCount; i++){
            ImageView imageView = (ImageView) onBoardingIndicatorLayout.getChildAt(i);
            if(i == index){
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(),R.drawable.shape_active_onboarding_indicator)
                );
            }
            else{
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(),R.drawable.shape_inactive_onboarding_indicator)
                );
            }
        }
        if(index == onBoardingViewPagerAdapter.getItemCount() - 1){
            onBoardingActionButton.setText(getString(R.string.ready));
        } else{
            onBoardingActionButton.setText(getString(R.string.next));
        }
    }


}