package com.mindgame.wordfi.controllers.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.button.MaterialButton;
import com.mindgame.wordfi.R;
import com.mindgame.wordfi.services.Screen;
/***
 * @author Rodrigue Ngalani Touko
 *
 *
 */
public class CustomRoundActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Enable Full Screen
        new Screen(this).fullMode();
        setContentView(R.layout.activity_custom_round);

        MaterialButton backButton = findViewById(R.id.custom_round_back_button);


        backButton.setOnClickListener(v -> {
            Intent intent=new Intent(this, DashboardActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });

    }
}