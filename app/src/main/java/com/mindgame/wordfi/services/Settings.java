package com.mindgame.wordfi.services;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;

import com.mindgame.wordfi.R;
import com.mindgame.wordfi.controllers.activities.WelcomeActivity;
import com.mindgame.wordfi.services.CoreData;

import java.util.Locale;

/**
 * @author Nils..
 *
 */
public class Settings {

    public static void setThemeTo(int value){
        if (value == 0) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }else if (value == 1){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
        }
    }


    public static void setLanguageTo(Context context, int value){

        String language;

        switch (value){
            case 0:
                language = "en";
                break;
            case 1:
                language = "de";
                break;
            case 2:
                language = "fr";
                break;
            default:
                language = Locale.getDefault().getLanguage();
                if (language.equals("fr")){
                    new CoreData(context).saveLanguage(2);
                }else if (language.equals("de")){
                    new CoreData(context).saveLanguage(1);
                }else{
                    new CoreData(context).saveLanguage(0);
                }
        }

        Locale locale = new Locale(language);
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        Locale.setDefault(locale);
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());


    }


    public static String getLevel(Context context){
        String level = "";
        int value = new CoreData(context).getLevel();
        String[] levels = context.getResources().getStringArray(R.array.level);
        switch (value){
            case 1:
                level = levels[1];
                break;
            case 2:
                level = levels[2];
                break;
            case 3:
                level = levels[3];
                break;
            default:
                level = levels[0];
        }
        return level;
    }

}

