package com.mindgame.wordfi.services;

import android.content.Context;
import android.media.MediaPlayer;

import com.mindgame.wordfi.R;
import com.mindgame.wordfi.controllers.activities.WelcomeActivity;

import static com.mindgame.wordfi.controllers.activities.WelcomeActivity.mediaPlayer;

public class SoundManager {


    public static void startBackgroundMusic(){
        mediaPlayer =  MediaPlayer.create(WelcomeActivity.context, R.raw.ukulele);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    public static void stopBackgroundMusic(){
        mediaPlayer.stop();
    }

    public static void mute(){
        mediaPlayer.setVolume(0f,0f);
    }

    public static void setVolumeTo(int params){
        float value = params / 100f;
        mediaPlayer.setVolume(value,value);
    }

    public static boolean isPlaying(){
      return mediaPlayer.isPlaying();
    }


}
